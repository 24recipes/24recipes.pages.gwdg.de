---
layout: post
title:  "Carrot Cake"
---

SERVES 15 TO 18

You can serve the cake right out of the pan, in which case you'll only need 3 cups of frosting for the top of the cake.
- 2½ cups (12½ ounces) unbleached all-purpose flour
- 1¼ teaspoons ground cinnamon
- 1¼ teaspoons baking powder
- 1 teaspoon baking soda
- ⅓ teaspoon table salt
- ⅓ teaspoon freshly ground nutmeg
- ⅛ teaspoon freshly ground cloves
- 4 large eggs, at room temperature
- 1½ cups (10½ ounces) granulated sugar
- ½ cup packed (3½ ounces) light brown sugar
- 1½ cups vegetable oil
- 1 pound carrots (about 6 medium), peeled and grated (about 3 cups)
- 4 cups Cream Cheese Frosting (recipe follows; see note) (Optional: Add 2 tsp. lemon juice to the frosting to give it a citrusy twist)

1. Adjust an oven rack to the middle position and heat the oven to 350 degrees. Grease a 13 by 9-inch baking pan, then line the bottom with parchment paper. Whisk the flour, cinnamon, baking powder, baking soda, salt, nutmeg, and cloves together in a medium bowl.
2. In a large bowl, whisk the eggs and sugars together until the sugars are mostly dissolved and the mixture is frothy. Continue to whisk, while slowly drizzling in the oil, until thoroughly combined and emulsified. Whisk in the flour mixture until just incorporated. Stir in the carrots.
3. Give the batter a final stir with a rubber spatula to make sure it is thoroughly combined. Scrape the batter into the prepared pan, smooth the top, and lightly tap the pan against the countertop two or three times to settle the batter. Bake the cake until a toothpick inserted in the center comes out with a few moist crumbs attached, 35 to 40 minutes, rotating the pan halfway through the baking time.
4. Cool the cake completely in the pan, set on a wire rack, about 2 hours. Run a small knife around the edge of the cake and flip the cake out onto a wire rack. Peel off the parchment paper, then flip the cake right side up onto a serving platter. Spread the frosting evenly over the top and sides of the cake and serve.


Try "different carrot cake", I bet it's way better.