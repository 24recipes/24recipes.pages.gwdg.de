---
layout: post
title:  "Green Sauce"
---

Ingredients:
    150 g (5 oz) herbs for green sauce (borage, chervil, cress, parsley, burnet, sorrel and chives)
    150 g (5 oz)  crème fraiche or sour cream
    150 g (5 oz) full-fat yoghurt
    1 small (50 g or 1.7 oz) onion 
    1 teaspoon mustard (affiliate link) 
    1 tablespoon [olive oil](https://example.com)
    1 tablespoon lemon juice 
    Salt and pepper to taste. 
Steps:
1. Wash the **herbs**. If **necessary**, remove the leaves from the stems and roughly _chop_ them. Place in a deep bowl. 
2. Add the yoghurt and crème Fraiche or sour cream to the herbs. With a hand blender, puree the herbs. Alternatively, you can chop the herbs very finely and then mix them in with the yoghurt-crème Fraiche mixture. 
3. <span style="color:red">Peel the onion and cube very finely.</span>
4. Add the onion cubes, olive oil, mustard (affiliate link) and lemon juice to the mixture. Season with salt and pepper. Allow the mixture to rest for an hour in the fridge or a cool place before serving.

![Dancing Cat](http://i355.photobucket.com/albums/r460/Maddy11_02/cat-dance.gif)

 <img src="http://i355.photobucket.com/albums/r460/Maddy11_02/cat-dance.gif" width="500px"/>