---
layout: post
title:  "Hot chocolate"
---

Some hot chocolate for cozy winterdays 

For 4 cups: 

- 200 g [dark chocolate](https://shop.rewe.de/p/rewe-beste-wahl-schokolade-edel-bitter-72-100g/3261314)
- 750 ml [milk](https://shop.rewe.de/p/weihenstephan-h-milch-3-5-1l/677067)
- 2 tsp [cocoa powder](https://shop.rewe.de/p/kaba-kakao-400g/7695121)
- a sprincling of [salt](https://shop.rewe.de/p/aquasale-meersalz-mit-jod-500g/3919102)
- 1-2 tbsp [sugar](https://shop.rewe.de/p/suedzucker-feinster-zucker-500g/7184011)
- [cream](https://shop.rewe.de/p/gluecksklee-spruehsahne-finesse-250ml/7685211)

1. First of all you need **200 g of dark chocolate**. 

2. You have to _chop_ it into coarse pieces. After that you put those pieces into a pot and _add_ a lillte **milk**, that they are slightly overcast. You _turn_ the hearth to a low state, that the **chocolate** can _melt slowly_.Thereby you have to _stir_ the **chocolate** in the **milk**. After the **chocolate** is melted you can _add_ the rest of the **750 ml of milk**.

3. While the **milk** warms up you continue to _stir_ and _add_ a little **sugar**, **cocoa powder**, **salt** and **cinnamon**. (The **cinnamon** will create a great smell).

4. After you filled your **hot chocolate** into a cup, you can _add_ some **cream** with a little more **cocoa powder** on it. 


![Homemade-Hot-Chocolate-4](https://celebratingsweets.com/wp-content/uploads/2018/12/Homemade-Hot-Chocolate-4.jpg)
