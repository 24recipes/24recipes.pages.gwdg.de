---
layout: post
title:  "Chocolate Cake"
---
<span style="color:pink">_Easy and delicious chocolate cake!_</span>

<h1><span style="color:hotpink">For the <b>cake </b>itself you will need:</span></h1>
<ul style="color:deeppink"> 

<li>400g flour</li>
<li>70g baking cocoa </li>
<li>360g sugar</li>
<li>2 teaspoons of vanilla extract</li>
<li>1 packet of baking soda</li>
<li>480ml water </li>
<li>200ml sunflower oil</li>

</ul>
<span style="color:MediumVioletRed">Mix all the dry ingriedients before adding the wet ingredients and mixing them in. When everything is mixed together, add it in a pan of your choosing and bake in a preheated oven at about 180 Degrees Celsius for about <b>35 to 40 minute</b>. If there is no uncooked batter left on a fork after stiking it into the cake it should be done cooking in the oven.</span>


<h2 style="color:coral">For the <b>frosting</b> you will need:</h2> 
<ul style="color:plum">

<li>1 cup of cocoa powder</li>
<li>1.5 cups of brown sugar (powdered)</li>
<li>1/3 cup of margarine</li>
<li>1 teaspoon of vanilla extract</li>
<li>1/2 cup of almond milk (or milk of your choice)</li>

</ul>

<ol style="color:dodgerblue"> 

<li>Mix dry ingredients in a bowl (cocoa powder & powdered sugar)</li>
<li>add wet ingredients (margarine, vanilla extract & almond milk)</li>
<li>If the frosting does not have the desired consistency, you can add more wet or dry ingredients</li>

</ol>

<span style="color:darkturquoise">
Afterwards you can [chill in the fridge for an hour][def]</span>

[def]: https://www.pinterest.de/pin/100134791698638977/

<img src="http://i355.photobucket.com/albums/r460/Maddy11_02/cat-dance.gif" width="500px"/>

![Dancing Cat][def2]

[def2]: http://i355.photobucket.com/albums/r460/Maddy11_02/cat-dance.gif