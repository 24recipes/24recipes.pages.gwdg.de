---
layout: post
title:  "Banana Bread"
---

Ingredients:
1. 113 g unsalted butter, soft or partially melted.
2. 3 large ripe bananas, completely brown if possible
3. 2 large eggs
4. [Vanilla extract or aroma.](https://sallysbakingaddiction.com/homemade-vanilla-extract/) (If you use extact, use approximately 1 tsp. If you use aroma, use approximately 4 drops.)
5. 250 g flour
6. 200 g sugar
7. 1 tsp Natron or baking soda
8. Approximately 1/2 tsp salt
9. 1/2 tsp cinnamon



_Instructions:_
Preheat the oven to 175º Celsius.
Combine the **wet ingredients** (butter, eggs, vanilla extract/aroma, and bananas mashed with a fork ) in a large bowl.
In a separate bowl, combine the **dry ingredients**.
Slowly add the dry ingredient mixture into the wet ingredient mixture, stirring as you combine them until completely mixed, with no chunks of the dry ingredients. The mixture will be fairly liquid, closer to cake batter than bread dough.
Use butter to coat the sides of a bread pan so the banana bread doesn't stick. 
Bake approximately 55 minutes until baked thoroughly. If you can insert a knife into the middle and no wet dough sticks to it, it's done. 
Serve after cooling or store in the fridge. 

![Dancing Cat](http://i355.photobucket.com/albums/r460/Maddy11_02/cat-dance.gif)