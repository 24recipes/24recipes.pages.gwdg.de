---
layout: post
title:  "Different Carrot Cake"
---

<p style="text-align: center;">#This is a
###different
##recipe for a carrot cake</p>

<p>My sister wrote this recipe when she was a kid.</p>
(note: for the heresaid *"cup"* we usually used **mugs**)

<p>3 carrots</p>
<p>1 cup of oil</p>
<p>1 spoon of baking powder (the German stuff)</p>
<p>2 cups of flour</p>
<p>1 cup of sugar</p>
<p>4 eggs</p>

---

<p>the original recipe has little pictures of eggs, carrots etc next to the ingredient, but you'll have to do without.</p>
<p>there is also no further information concerning mixing and baking (temperature, duration), so you'll have to figure it out for yourself.</p>
<p>happy baking!</p>