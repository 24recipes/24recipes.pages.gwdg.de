[![Build Status](https://gitlab.com/pages/jekyll/badges/master/pipeline.svg)](https://gitlab.com/pages/jekyll/-/pipelines?ref=master)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

# Recipes playground for git versioning

## Getting started

1. Download and install [GitHub Desktop](https://desktop.github.com/)
2. Clone the project/repository <https://gitlab.gwdg.de/24recipes/24recipes.pages.gwdg.de>

## Syncing and editing files

1. Create a new branch.
2. Open the project/repository in Visual Studio Code.
3. Edit one of the files in `_posts_` or create a new one with the name and date, such as `2024-05-06-cookies.md`
4. Commit changes.
5. Publish your branch (using the password in [OLAT](https://olat-ce.server.uni-frankfurt.de/olat/auth/RepositoryEntry/20670545926/CourseNode/1713408124938163007) and any username).

## Setting up an account

1. Log into <https://academiccloud.de/> using "Federated Login" > "Universität Frankfurt". 
2. After you're logged in, go to <https://id.academiccloud.de/account>. Copy the "username" and "Academic ID" that appears there and send them to me. I'll request access for you. 
3. Once I let you know you have access, sign in at <https://gitlab.gwdg.de/users/sign_in?redirect_to_referer=yes>. 

## Merge request

1. Once you have changes you want to merge, go to <https://gitlab.gwdg.de/24recipes/24recipes.pages.gwdg.de/-/merge_requests> and click "New merge request." Select your branch as the "source branch" and "master" as the "target branch. 
2. Click "Compare changes" and then "Create merge request."
